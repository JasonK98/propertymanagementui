export interface Property {
    name: string,
    price: string,
    location: string,
    image: string
};
