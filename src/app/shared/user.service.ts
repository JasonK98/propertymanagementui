import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {  Response } from "@angular/http";
import { User } from './user.model';

@Injectable()
export class UserService {
  readonly rootUrl = 'http://propertymanageapi.azurewebsites.net';
  constructor(private http: HttpClient) { }


  testApi() {
    console.log( "testAPI" );
    console.log( this.rootUrl + '/api/values' );
    return this.http.get( this.rootUrl + '/api/values' );
  }

  registerUser(user : User){
    const body: User = {
      UserName: user.UserName,
      Password: user.Password,
      Email: user.Email,
      FirstName: user.FirstName,
      LastName: user.LastName,
      FullName: user.FirstName + user.LastName
    }
    return this.http.post(this.rootUrl + '/api/ApplicationUser/Register', body);
  }

  login(formData) {
    return this.http.post(this.rootUrl + '/api/ApplicationUser/Login', formData);
  }

}
