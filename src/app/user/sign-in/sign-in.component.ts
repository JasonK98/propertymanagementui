import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/user.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  constructor(private userService: UserService, private router: Router, private toastrService: ToastrService) { }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm) {
    this.userService.login( form.value )
      .subscribe((res:any) => {
          // Store JSON web token in local storage
        localStorage.setItem('token', res.token);
        this.router.navigateByUrl('/home');
      }, (err) => {
        if (err.status == 400) {
          this.toastrService.error('Invalid username or password.', 'Login Failed');
        } else {
          console.log(err);
        }
      });
  }

}
