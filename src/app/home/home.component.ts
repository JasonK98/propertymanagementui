import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PropertyService } from '../shared/property.service';
import { Property } from '../shared/property.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  properties: any = [];

  constructor(private router: Router, private propertyService: PropertyService) { }



  ngOnInit(): void {
    this.getProperties();
  }

  onLogout() {
    localStorage.removeItem('token');
    this.router.navigateByUrl('/login');
  }

  getProperties() {
    this.propertyService.getProperties()
      .subscribe(data => {
        this.properties = data;
      });
  }

}
